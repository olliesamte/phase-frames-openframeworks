//
//  udpPacketToFrameBuilder.cpp
//  imageSequenceExample
//
//  Created by James Murchison on 21/04/2015.
//
//

#include "udpPacketToFrameBuilder.h"
#include "defines.h"
#include "utils/ofUtils.h"

udpPacketToFrameBuilder::udpPacketToFrameBuilder(int _frameNumber, int _targetPackets) {
    frameNumber = _frameNumber;
    targetPackets = _targetPackets;
    packets = new std::map<int, char*>();
    packetSizes = new std::map<int, int>();
}

udpPacketToFrameBuilder::~udpPacketToFrameBuilder() {
    for(std::map<int, char*>::iterator it = packets->begin(); it != packets->end(); ++it) {
        if(it->second != NULL) {
            delete[] it->second;
            it->second = NULL;
        } else {
//            cout << "Trying to delete NULL element\n";
        }
    }
    delete packets;
    packets = NULL;
    delete packetSizes;
    packetSizes = NULL;
}

//This is a CLEAN copy of the packet, no mucking
void udpPacketToFrameBuilder::addPacket(char *packet, int packetNumber, int packetSize) {
    char* packetClone = new char[packetSize];
    int i = 0;
    for(i = 0; i < packetSize; i++) {
        packetClone[i] = packet[i];
    }
    packets->insert(std::make_pair(packetNumber, packetClone));
    packetSizes->insert(std::make_pair(packetNumber, packetSize));
}

bool udpPacketToFrameBuilder::isBufferReady() {
//    cout << "Checking if buffer is ready, packet size=" << packets->size() << "/" << targetPackets << "\n";
    return packets->size() == targetPackets;
}

constructedFrame udpPacketToFrameBuilder::constructFrame() {
    int frameSize = 0;
    for (std::map<int, int>::iterator it = packetSizes->begin(); it != packetSizes->end(); ++it) {
        frameSize += it->second - EXTRA_BUFFER_DATA_SIZE;
    }
    constructedFrame frame;
    frame.frameLength = frameSize;
    frame.frameData = new char[frameSize];
    int i = 0;
    int count = 0;
    for(std::map<int, char*>::iterator it = packets->begin(); it != packets->end(); ++it) {
        for(i = 0; i < packetSizes->find(it->first)->second - EXTRA_BUFFER_DATA_SIZE; ++i) {
            frame.frameData[count + i] = it->second[i + EXTRA_BUFFER_DATA_SIZE];
        }
        count += (packetSizes->find(it->first)->second - EXTRA_BUFFER_DATA_SIZE);
    }
    return frame;
}