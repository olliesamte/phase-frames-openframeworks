#include "ofApp.h"


/*
 
 Image Sequence Example:
 In this example we are going to load a sequence of images from a folder.
 We know that the images are in order and the we saved them out at 24fps. 
 the goal is to play them back at 24fps independent of the fps of the app.
 You can toggle the sequence to be independent of the app fps.
 
 Topics:
 - ofDirectory
 - ofImage
 - timing
 
 gif from: http://probertson.livejournal.com/32350.html
 
 */


//--------------------------------------------------------------
void ofApp::setup() {
    
    ofBackground(0);
    
    // this toggle will tell the sequence
    // be be indepent of the app fps
    bFrameIndependent = true;
    
    currentImage = new ofImage();
    ofColor c = ofColor();
    c.set(0.0, 0.0, 0.0);
    currentImage->setColor(c);
    
    sequenceFPS = 15;

    // set the app fps 
    appFPS = sequenceFPS;
    ofSetFrameRate(appFPS);
    
    // initialize the network controller
    nc = new networkController();
    nc->initialize();
    
    //hide the cursor
    cout << "Hiding cursor\n";
    ofHideCursor();
    
}

//--------------------------------------------------------------
void ofApp::update() {
//    cout <<  "Updating\n";
    nc->update();
    if(nc->hasNewImageData()) {              
        currentImageBuffer = new ofBuffer(nc->getImageData(), nc->getImageDataSize());
        currentImage->loadImage(*currentImageBuffer);
        currentImageBuffer->clear();
        delete currentImageBuffer;
        currentImageBuffer = NULL;
    }
//    cout <<  "Done updating\n";
}

//--------------------------------------------------------------
void ofApp::draw() {
    
//    cout << "Drawing\n";
    // draw the image sequence at the new frame count
    ofSetColor(255);

    
    currentImage->draw(0, 0, 1920, 1080);
    if(CHECK_CAMERA) {
        nc->cameraImage.draw(0, 0, 640, 320);
    }

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
    
}
