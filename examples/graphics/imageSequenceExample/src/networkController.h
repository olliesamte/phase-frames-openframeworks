//
//  networkController.h
//  imageSequenceExample
//
//  Created by James Murchison on 21/04/2015.
//
//

#ifndef __imageSequenceExample__networkController__
#define __imageSequenceExample__networkController__

#include <stdio.h>
#include "ofMain.h"
#include "ofxNetwork.h"
#include "ofxRPiCameraVideoGrabber.h"
#include "ofImage.h"
#include "udpPacketToFrameBuilder.h"
#include "defines.h"

class networkController {
private:
    
    char* udpMessage;
    
    int writeIndex;
    int imageDataSize[IMAGE_DATA_BUFFER_LENGTH];
    char imageData[IMAGE_DATA_BUFFER_LENGTH][MAX_IMAGE_SIZE];
    bool hasImageData;
    
    char *oneShotImageData;
    int oneShotImageDataSize;
    
    std::map<int, udpPacketToFrameBuilder*>* frameBuffers;
    
    int delay;
    
    char* udpPingMessage;
    bool newPingData;
    long timeOffsetServer;
    
    char* udpCommandMessage;
    string* commandMessageData;
    bool newCommand;
    
    int myID;
    
    unsigned int latestFrame;
    
    //video grabber stuff
    ofxRPiCameraVideoGrabber videoGrabber;
    ofBuffer cameraSendBuffer;
    
public:
    
    ofImage cameraImage;
    
    unsigned long lastContact;
    
    void initialize();
    void update();
    void doTextureCompression();
    
    void convert(ofImage &, ofBuffer & );
    
    char* getImageData();
    int getImageDataSize();
    bool hasNewImageData();
    
    bool hasNewCommand();
    string* getCommandMessage();
    
    char * frameSendData;
    int compressedDataSize;
    
    bool ignoreCamera;
    
    ofxUDPManager udpImagesConnection;
    ofxUDPManager udpControlConnection;
    ofxUDPManager udpPingConnection;
    
    ofxUDPManager udpFrameSender;
    
};

#endif /* defined(__imageSequenceExample__networkController__) */
