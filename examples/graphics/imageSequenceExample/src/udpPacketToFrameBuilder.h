//
//  udpPacketToFrameBuilder.h
//  imageSequenceExample
//
//  Created by James Murchison on 21/04/2015.
//
//

#ifndef __imageSequenceExample__udpPacketToFrameBuilder__
#define __imageSequenceExample__udpPacketToFrameBuilder__

#include <stdio.h>
#include <map>
#include "structs.h"

class udpPacketToFrameBuilder {
private:
    int frameNumber;
    unsigned int targetPackets;
    std::map<int, char*>* packets;
    std::map<int, int>* packetSizes;

public:
    udpPacketToFrameBuilder(int frameNumber, int targetPackets);
    ~udpPacketToFrameBuilder();
    void addPacket(char* packet, int packetNumber, int packetSize);
    bool isBufferReady();
    constructedFrame constructFrame();
};

#endif /* defined(__imageSequenceExample__udpPacketToFrameBuilder__) */
