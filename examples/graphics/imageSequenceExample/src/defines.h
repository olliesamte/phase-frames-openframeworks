//
//  defines.h
//  imageSequenceExample
//
//  Created by James Murchison on 23/04/2015.
//
//

#ifndef imageSequenceExample_defines_h
#define imageSequenceExample_defines_h

#define EXTRA_BUFFER_DATA_SIZE 12
#define MAX_BUFFER_SIZE 65000 - EXTRA_BUFFER_DATA_SIZE

#define MAX_IMAGE_SIZE  1000000
#define UDP_BUFFER_SIZE 100000
#define UDP_PING_BUFFER_SIZE 8
#define UDP_COMMAND_BUFFER_SIZE 4000

#define MULTICAST_STREAM_PORT 9000
#define MULTICAST_PING_PORT 9001
#define MULTICAST_CONTROL_PORT 9002

#define SEND true
#define TEXTURE_COMPRESSION false
#define USE_DELAYS true

#define MULTICAST_ADDRESS "224.1.1.1"

#define IMAGE_DATA_BUFFER_LENGTH 500

#define MOVIE_WIDTH 640
#define MOVIE_HEIGHT 320
#define FPS 15
#define BMP_SIZE 614400

//#define FRAME_SEND_HOST "pf-server.local"
#define FRAME_SEND_HOST "192.168.1.2"
#define FRAME_SEND_PORT_START 10000


#endif
