//
//  networkController.cpp
//  imageSequenceExample
//
//  Created by James Murchison on 21/04/2015.
//
//

//#include "GL/glew.h"
#include "networkController.h"
#include "utils/ofUtils.h"

char* networkController::getImageData() {
    hasImageData = false;
    if(USE_DELAYS) {
        int frameToGet = (writeIndex - delay + IMAGE_DATA_BUFFER_LENGTH) % IMAGE_DATA_BUFFER_LENGTH;
        return imageData[frameToGet];
    } else {
        return oneShotImageData;
    }
}

int networkController::getImageDataSize() {
    if(USE_DELAYS) {
        int frameToGet = (writeIndex - delay + IMAGE_DATA_BUFFER_LENGTH) % IMAGE_DATA_BUFFER_LENGTH;
        return imageDataSize[frameToGet];
    } else {
        return oneShotImageDataSize;
    }
}

bool networkController::hasNewImageData() {
    return hasImageData;
}

bool networkController::hasNewCommand() {
    return newCommand;
}

string* networkController::getCommandMessage() {
    newCommand = false;
    return commandMessageData;
}

void networkController::initialize() {
    
    //all the hostnames
    map<string, int> hostnames;
    hostnames.insert(pair<string,int>("b827eb1958ea",0));
    hostnames.insert(pair<string,int>("b827eb0aa031",1));
    hostnames.insert(pair<string,int>("b827eb3e923f",2));
    hostnames.insert(pair<string,int>("b827eb5b3a47",3));
    hostnames.insert(pair<string,int>("b827eb19eb64",4));
    hostnames.insert(pair<string,int>("b827ebf4c035",5));
    hostnames.insert(pair<string,int>("b827eb658d29",6));
    hostnames.insert(pair<string,int>("b827ebd40add",7));
    hostnames.insert(pair<string,int>("b827ebd3c42f",8));
    hostnames.insert(pair<string,int>("b827ebdf8039",9));
    hostnames.insert(pair<string,int>("b827ebc93d8d",10));
    hostnames.insert(pair<string,int>("b827eb9d001a",11));
    hostnames.insert(pair<string,int>("b827eb78be42",12));
    hostnames.insert(pair<string,int>("b827eb155ae2",13));
    hostnames.insert(pair<string,int>("b827eb9eb223",14));
    hostnames.insert(pair<string,int>("b827ebb8ec53",15));
    hostnames.insert(pair<string,int>("b827ebc6f4ae",16));
    hostnames.insert(pair<string,int>("b827ebd77d1c",17));
    
    //find out my hostname
    string myHostname;
    ifstream myfile ("/etc/hostname");
    if (myfile.is_open())
    {
        getline (myfile, myHostname);
    }
    cout << "HOSTNAME IS: " << myHostname << '\n';
    myfile.close();
    //get my ID from hostname
    myID = hostnames.find(myHostname)->second;
    cout << "My ID is " << myID << "\n";
    
    
    ignoreCamera = true;
    
    if(SEND) {
        
        try {
            
            //set up video grabber stuff
            OMXCameraSettings omxCameraSettings;
            omxCameraSettings.width = MOVIE_WIDTH;
            omxCameraSettings.height = MOVIE_HEIGHT;
            omxCameraSettings.framerate = FPS;
            omxCameraSettings.isUsingTexture = true;
            omxCameraSettings.enablePixels = true;
            omxCameraSettings.doRecording = false;
            
            cout << "Set OMXCameraSettings, now setting up videoGrabber\n";
            videoGrabber.setup(omxCameraSettings);
            cout << "Set up videoGrabber\n";
            
            ignoreCamera = false;
            
        } catch(...) {
            cout << "CAMERA CONNECTION FAILURE: Unable to connect to camera\n";
            ignoreCamera = true;
        }
    }
    
    //set up video frame sending data structure
    frameSendData = new char[UDP_BUFFER_SIZE];
    
    //set up networky stuff
    frameBuffers = new map<int, udpPacketToFrameBuilder*>();
    
    //set up listening data structures
    udpMessage = new char[UDP_BUFFER_SIZE];
    udpPingMessage = new char[UDP_PING_BUFFER_SIZE];
    udpCommandMessage = new char[UDP_COMMAND_BUFFER_SIZE];
    delay = 2;
    hasImageData = false;
    
    //set up listeners
    char multiCastAddress[] = {MULTICAST_ADDRESS};
    
    udpControlConnection.Create();
    udpControlConnection.BindMcast(multiCastAddress, MULTICAST_CONTROL_PORT);
    udpControlConnection.SetNonBlocking(true);
    
    udpPingConnection.Create();
    udpPingConnection.BindMcast(multiCastAddress, MULTICAST_PING_PORT);
    udpPingConnection.SetNonBlocking(true);
    
    udpImagesConnection.Create();
    udpImagesConnection.BindMcast(multiCastAddress, MULTICAST_STREAM_PORT);
    udpImagesConnection.SetNonBlocking(true);
    
    //set up sender
    
    char frameSendHost[] = {FRAME_SEND_HOST};
    udpFrameSender.Create();
    bool isConnected = udpFrameSender.Connect(frameSendHost, FRAME_SEND_PORT_START + myID);
    if(!isConnected) {
        cout << "Connection failed\n";
    } else {
        cout << "Got connection to send frames!\n";
    }
}

void networkController::doTextureCompression() {
//    GLuint mytex = (GLuint)videoGrabber.getTextureReference().getTextureData().textureID;
//    //assumes we have some stuff like a texture, frame buffer object, etc.
//    GLuint myrbo, myfbo;
//    glGenTextures(1, &mytex);
//    glBindTexture(GL_TEXTURE_2D, mytex);
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA, MOVIE_WIDTH, MOVIE_HEIGHT, 0,
//                 GL_RGBA, GL_UNSIGNED_BYTE, 0 );
//    glGenRenderbuffers(1, &myrbo);
//    glBindRenderbuffer(GL_RENDERBUFFER, myrbo);
//    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, MOVIE_WIDTH, MOVIE_HEIGHT);
//    
//    glGenFramebuffers(1, &myfbo);
//    glBindFramebuffer(GL_FRAMEBUFFER, myfbo);
//    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
//                              GL_RENDERBUFFER, myrbo);
//    // If you need a Z Buffer:
//    // create a 2nd renderbuffer for the framebuffer GL_DEPTH_ATTACHMENT
//    
//    // render (i.e. create the data for the texture)
//    
//    // Now get the data out of the framebuffer by requesting a compressed read
//    glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA,
//                     0, 0, MOVIE_WIDTH, MOVIE_HEIGHT, 0);
//    glBindFramebuffer(GL_FRAMEBUFFER, 0);
//    glBindRenderbuffer(GL_RENDERBUFFER, 0);
//    glDeleteRenderbuffers(1, &myrbo);
//    glDeleteFramebuffers(1, &myfbo);
//    //so if I understand, myfbo now contains the compressed data??
//    // Validate it's compressed / read back compressed data
//    GLint format = 0;
//    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &format);
//    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE,&compressedDataSize);
//    
//    cout << "Grabbing compressed frame data, size=" << compressedDataSize << "\n";
//    if(compressedDataSize <= UDP_BUFFER_SIZE) {
//        glGetCompressedTexImage(GL_TEXTURE_2D, 0, frameSendData);
//        cout << "Got the data\n";
//    } else {
//        cout << "Could not compress frame to under frame buffer size\n";
//    }
//    glBindTexture(GL_TEXTURE_2D, 0);
//    glDeleteTextures(1, &mytex);
//    // data now contains the compressed thing!!
}

void networkController::update() {
    
//    cout << "Starting controller cycle\n";
    
    int received;
    bool frameRequested = false;
    received = udpControlConnection.Receive(udpCommandMessage, UDP_COMMAND_BUFFER_SIZE);
    if(received == SOCKET_ERROR || received == SOCKET_TIMEOUT) {
        // ERROR or Timeout
    } else {
//        printf("Received control message %s\n", udpCommandMessage);
        if(udpCommandMessage[0] == 'd') {
            //delay and frame request message
            //next 18 chars after the 'd' indicate frame requests, 't', or 'f'.
            frameRequested = udpCommandMessage[myID+1] == 't';
            //now carry on, nothing has been changed in the array
            istringstream iss(udpCommandMessage);
            string s;
            int count = 0;
            while ( getline( iss, s, ' ' ) ) {
                if(count == myID + 1) {
                    delay = ofToInt(s.c_str());
//                    cout << "DELAY VALUE " << delay << "\n";
                    break;
                }
                count++;
            }
        }
        //////////////////////////////////
        newCommand = true;
        lastContact = ofGetElapsedTimeMillis();
    }
    
    //    cout << "Update\n";
    
    if(frameRequested && SEND && !ignoreCamera) {
//        cout << "FRAME REQUESTEDD\n";
//        cout << "Seeking camera frame\n";
        //grab pixels from camera
        if(!videoGrabber.isFrameNew()) {
//            cout << "No image data\n";
        } else {
            if(TEXTURE_COMPRESSION) {
                //the texture version
//              doTextureCompression();
            } else {
                
                //TODO: run this on a separate thread with delay?
                
                //the pixels version
                cameraImage.setFromPixels(videoGrabber.getPixels(), MOVIE_WIDTH, MOVIE_HEIGHT, OF_IMAGE_COLOR_ALPHA);
                cameraImage.setImageType(OF_IMAGE_GRAYSCALE);
                ofSaveImage(cameraImage.getPixelsRef(),cameraSendBuffer,OF_IMAGE_FORMAT_JPEG,OF_IMAGE_QUALITY_MEDIUM);
                frameSendData = cameraSendBuffer.getBinaryBuffer();
                compressedDataSize = cameraSendBuffer.size();
//                cout << "Sending compressed image of size " << compressedDataSize << "\n";
                // now send over UDP
                if(compressedDataSize <= UDP_BUFFER_SIZE) {
                    udpFrameSender.Send(frameSendData, compressedDataSize);
                } else {
//                    cout << "Could not send compressed camera image, too big! Size is " << compressedDataSize << ", capacity is " << UDP_BUFFER_SIZE << ".\n";
                }
                
            }
        }
    } else {
//        cout << "NO FRAME REQUEST\n";
    }
    
    received = udpPingConnection.Receive(udpPingMessage, UDP_PING_BUFFER_SIZE);
    if(received == SOCKET_ERROR || received == SOCKET_TIMEOUT) {
        // ERROR or Timeout
    } else {
        pingMessage* pm = (pingMessage*)udpPingMessage;
        timeOffsetServer = pm->time - ofGetSystemTime();
//        printf("Received ping from server. Server time is : %ld\n", pm->time);
        
        //TODO: Check we're alive and resolve issues if not.
        
        lastContact = ofGetElapsedTimeMillis();
    }
    
    int count = 0;
    
    while(count++ < 100) {
        received = udpImagesConnection.Receive(udpMessage, UDP_BUFFER_SIZE);
        if(received == SOCKET_ERROR || received == SOCKET_TIMEOUT || received == 0) {
            // ERROR or Timeout
            break;
        } else {
            // Got a packet
            packetHeader* ph = (packetHeader*)udpMessage;
            if(latestFrame < ph->frameNumber || ph->frameNumber < 500) {            //risky?
                latestFrame = ph->frameNumber;
            }
            udpPacketToFrameBuilder* packetBuilder = NULL;
    //        cout << "Got a packet, packet number: " << ph->packetNumber << "\n";
            if(frameBuffers->count(ph->frameNumber) == 0) {
//                cout << "Creating new packet builder for frame " << ph->frameNumber << " (latest frame = " << latestFrame << ")\n";
                packetBuilder = new udpPacketToFrameBuilder(ph->frameNumber, ph->totalPackets);
                frameBuffers->insert(std::make_pair(ph->frameNumber, packetBuilder));
            } else {
                packetBuilder = frameBuffers->find(ph->frameNumber)->second;
            }
            packetBuilder->addPacket(udpMessage, ph->packetNumber, received);
//            cout << "Added packet, " << "frameno=" << ph->frameNumber << ", packetno=" << ph->packetNumber << "/" << ph->totalPackets << ", size=" << received << " " << ofToHex((udpMessage + sizeof(packetHeader))[0]) << " " << ofToHex((udpMessage + sizeof(packetHeader))[1]) << "\n";
            std::map<int, udpPacketToFrameBuilder*>::iterator it = frameBuffers->begin();
            while(it != frameBuffers->end()) {
                //first run clean up check: remove any data stored in frames older than 3 frames ago
                if(it->first < latestFrame - 20) {
//                    cout << "Destroying an old frame.\n";
                    //destroy the entry
                    delete it->second;
                    frameBuffers->erase(it++);
                } else if(it->second->isBufferReady()) {
                    //once here we will delete this entry
    //                cout << "Buffer is ready " << it->first << "\n";
                    constructedFrame newFrame = it->second->constructFrame();   //we construct a frame here, so it should be deleted
                    // Got a packet
                    if(USE_DELAYS) {
    //                    cout << "Copying\n";
                        memcpy(imageData[writeIndex], newFrame.frameData,  newFrame.frameLength);
//                        cout << "Done copying, length=" << newFrame.frameLength << " " << ofToHex(newFrame.frameData[0]) << " " << ofToHex(newFrame.frameData[1]) << "\n";
                        imageDataSize[writeIndex] = newFrame.frameLength;
                    } else {
                        oneShotImageData = newFrame.frameData;
                        oneShotImageDataSize = newFrame.frameLength;
                    }
                    //increment write index
                    writeIndex = (writeIndex + 1) % IMAGE_DATA_BUFFER_LENGTH;
                    hasImageData = true;
//                    printf("Image packet received length: %d fn: %lu pn: %lu tp:%lu \n\r", received, ph->frameNumber, ph->packetNumber, ph->totalPackets);
                    delete it->second;
                    frameBuffers->erase(it++);
//                    cout << "Deleted frame buffer contents, now to delete newFrame\n";
                    delete[] newFrame.frameData;            //TODO: check that this is working
//                    cout << "Done that too :-)\n";
                } else {
                    ++it;
                }
            }
            lastContact = ofGetElapsedTimeMillis();
        }
//        cout << "End of while loop\n";
    }
    //check how many udpPacketToFrameBuilders are still being kept
//    cout << "There are currently " << frameBuffers->size() << " udpPacketToFrameBuilder objects active.\n";
//    cout << "Done with image code\n";
    
    //now check if no contact
    if((ofGetElapsedTimeMillis() > 60000) && (ofGetElapsedTimeMillis() - lastContact > 10000)) {    //10s timeout
        cout << "Detected that there are no network messages!\n";
        //exit(0);            //TEST CODE, SEE IF WE GET A SHUTDOWN HAPPENING.
        //system("sudo killall -KILL phaseApp");
    }
}



