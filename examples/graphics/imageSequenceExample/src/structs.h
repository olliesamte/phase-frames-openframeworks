//
//  structs.h
//  imageSequenceExample
//
//  Created by James Murchison on 23/04/2015.
//
//

#ifndef imageSequenceExample_structs_h
#define imageSequenceExample_structs_h

struct packetHeader {
    unsigned long frameNumber;
    unsigned long packetNumber;
    unsigned long totalPackets;
};

struct pingMessage {
    long int time;
};

struct constructedFrame {
    int frameLength;
    char* frameData;
};

#endif
